import lombok.Data;

@Data
public class OtherCodeType {
    private String Code;
    private String Name;
    private String Description;
}
