import lombok.Data;

@Data
public class Site {
    private String Identification;
    private String Name;
}
