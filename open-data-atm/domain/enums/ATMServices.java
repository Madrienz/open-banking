package enums;

public enum ATMServices {
    ATBA("Balance"),
    ATBP("BillPayments"),
    ATCA("CashDeposits"),
    ATCD("CharityDonation"),
    ATCQ("ChequeDeposits"),
    ATCW("CashWithdrawal"),
    ATED("EnvelopeDeposit"),
    ATFC("FastCash"),
    ATMB("MobileBankingRegistration"),
    ATMP("MobilePaymentRegistration"),
    ATOS("OrderStatement"),
    ATOT("Other"),
    ATPA("PINActivation"),
    ATPC("PINChange"),
    ATPU("PINUnblock"),
    ATTS("MiniStatement");

    private String value;

    ATMServices(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }

}
