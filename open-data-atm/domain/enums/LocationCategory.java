package enums;

public enum LocationCategory {
    ATBE("BranchExternal"),
    ATBI("BranchInternal"),
    ATBL("BranchLobby"),
    ATOT("Other"),
    ATRO("RetailerOutlet"),
    ATRU("RemoteUnit");

    private String value;

    LocationCategory(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }

}
