package ccc;

import core.CoreProduct;
import eligibility.Eligibility;
import enums.MarketingState;
import enums.Period;
import features.FeaturesAndBenefits;
import lombok.Data;
import other.OtherFeeCharges;
import repayment.Repayment;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Data
@Entity
@Table(name = "ccc_marketing_state")
public class CccMarketingState {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(length = 35, nullable = false)
    private String identification;

    @Column(name = "predecessor_id", length = 35)
    private String predecessorId;

    @Enumerated(EnumType.STRING)
    @Column(length = 4, nullable = false)
    private MarketingState marketingState;

    @Column(name = "first_marketed_date")
    private LocalDate firstMarketedDate = LocalDate.of(1900, 1,1);

    @Column(name = "last_marketed_date")
    private LocalDate lastMarketedDate = LocalDate.of(9999, 12,31);

    @Column(name = "state_tenure_length")
    private int stateTenureLength;

    @Enumerated(EnumType.STRING)
    @Column(name = "state_tenure_period")
    private Period stateTenurePeriod;

    @ElementCollection
    @CollectionTable(name = "ccc_marketing_state_notes",
            joinColumns = @JoinColumn(name = "ccc_marketing_state_id"))
    private List<String> notes;

    @OneToOne
    @JoinColumn(name = "repayment_id")
    private Repayment repayment;

    @OneToOne
    @JoinColumn(name = "eligibility_id", nullable = false)
    private Eligibility eligibility;

    @OneToOne
    @JoinColumn(name = "features_and_benefits_id", nullable = false)
    private FeaturesAndBenefits featuresAndBenefits;

    @OneToOne
    @JoinColumn(name = "other_fee_charges_id", nullable = false)
    private OtherFeeCharges otherFeeCharges;

    @OneToOne
    @JoinColumn(name = "core_product_id", nullable = false)
    private CoreProduct coreProduct;

    @ManyToOne
    @JoinColumn(name = "ccc_id")
    private Ccc ccc;
}
