package features;

import enums.FeatureBenefitType;
import enums.Frequency;
import lombok.Data;
import other.OtherCodeType;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "features_benefit_group")
public class FeatureBenefitGroup {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(length = 350, nullable = false)
    private String name;

    @Enumerated(EnumType.STRING)
    @Column(length = 4)
    private FeatureBenefitType type;

    @Column(name = "benefit_group_nominal_value")
    private String benefitGroupNominalValue;

    @Column(name = "fee")
    private String fee;

    @Enumerated(EnumType.STRING)
    @Column(name = "application_frequency", length = 4)
    private Frequency applicationFrequency;

    @Enumerated(EnumType.STRING)
    @Column(name = "calculation_frequency", length = 4)
    private Frequency calculationFrequency;

    @ElementCollection
    @CollectionTable(name = "features_benefit_group_notes",
            joinColumns = @JoinColumn(name = "features_benefit_group_id"))
    private List<String> notes;

    @ElementCollection
    @CollectionTable(name = "features_benefit_group_other_type",
            joinColumns = @JoinColumn(name = "features_benefit_group_id"))
    private OtherCodeType otherType;

    @ElementCollection
    @CollectionTable(name = "features_benefit_group_other_application_frequency",
            joinColumns = @JoinColumn(name = "features_benefit_group_id"))
    private OtherCodeType otherApplicationFrequency;

    @ElementCollection
    @CollectionTable(name = "features_benefit_group_other_calculation_frequency",
            joinColumns = @JoinColumn(name = "features_benefit_group_id"))
    private OtherCodeType otherCalculationFrequency;

    @OneToMany(mappedBy = "features_benefit_group_id")
    private List<FeatureBenefitItem> featureBenefitItem;

    @OneToMany(mappedBy = "features_benefit_group_id")
    private List<FeatureBenefitEligibility> featureBenefitEligibility;

    @ManyToOne
    @JoinColumn(name = "features_and_benefits_id")
    private FeaturesAndBenefits featuresAndBenefits;
}
