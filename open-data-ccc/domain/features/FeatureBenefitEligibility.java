package features;

import enums.EligibilityType;
import enums.Period;
import lombok.Data;
import other.OtherCodeType;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "feature_benefit_eligibility")
public class FeatureBenefitEligibility {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(length = 350, nullable = false)
    private String name;

    @Column(length = 500)
    private String description;

    @Enumerated(EnumType.STRING)
    @Column(length = 4)
    private EligibilityType type;

    @Column
    private String amount;

    @Column
    private boolean indicator;

    @Column(length = 500)
    private String textual;

    @Enumerated(EnumType.STRING)
    @Column(length = 4)
    private Period period;

    @ElementCollection
    @CollectionTable(name = "feature_benefit_eligibility_notes",
            joinColumns = @JoinColumn(name = "feature_benefit_eligibility_id"))
    private List<String> notes;

    @ElementCollection
    @CollectionTable(name = "feature_benefit_eligibility_other_type",
            joinColumns = @JoinColumn(name = "feature_benefit_eligibility_id"))
    private OtherCodeType otherType;

    @ManyToOne
    @JoinColumn(name = "features_benefit_item_id")
    private FeatureBenefitItem featureBenefitItem;
}
