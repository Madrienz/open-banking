package eligibility;

import enums.EligibilityType;
import enums.Frequency;
import lombok.Data;
import other.OtherCodeType;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "other_eligibility")
public class OtherEligibility {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(length = 350, nullable = false)
    private String name;

    @Column(length = 500, nullable = false)
    private String description;

    @Enumerated(EnumType.STRING)
    @Column(length = 4, nullable = false)
    private EligibilityType type;

    @Column
    private String amount;

    @Column
    private boolean indicator;

    @Column(length = 500)
    private String textual;

    @Enumerated(EnumType.STRING)
    @Column(length = 4)
    private Frequency period;

    @ElementCollection
    @CollectionTable(name = "other_eligibility_notes",
            joinColumns = @JoinColumn(name = "other_eligibility_id"))
    private List<String> notes;

    @ElementCollection
    @CollectionTable(name = "other_eligibility_other_types", joinColumns = @JoinColumn(name = "other_eligibility_id"))
    private OtherCodeType otherType;

    @ManyToOne
    @JoinColumn(name = "eligibility_id")
    private Eligibility eligibility;
}
