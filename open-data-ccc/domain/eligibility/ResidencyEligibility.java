package eligibility;

import enums.ResidencyType;
import lombok.Data;
import other.OtherCodeType;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "residency_eligibility")
public class ResidencyEligibility {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "residency_type")
    private ResidencyType residencyType;

    @Column(name = "residency_included")
    private String residencyIncluded;

    @ElementCollection
    @CollectionTable(name = "residency_eligibility_notes",
            joinColumns = @JoinColumn(name = "residency_eligibility_id"))
    private List<String> notes;

    @ElementCollection
    @CollectionTable(name = "residency_eligibility_other_residency_types",
            joinColumns = @JoinColumn(name = "residency_eligibility_id"))
    private OtherCodeType otherResidencyType;

    @ManyToOne
    @JoinColumn(name = "eligibility_id")
    private Eligibility eligibility;
}
