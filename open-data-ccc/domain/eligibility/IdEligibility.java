package eligibility;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "id_eligibility")
public class IdEligibility {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column
    private String url;

    @ElementCollection
    @CollectionTable(name = "id_eligibility_notes",
            joinColumns = @JoinColumn(name = "id_eligibility_id"))
    private List<String> notes;

    @OneToOne
    @JoinColumn(name = "eligibility_id")
    private Eligibility eligibility;
}
