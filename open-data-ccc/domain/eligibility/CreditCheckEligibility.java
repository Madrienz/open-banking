package eligibility;

import enums.CreditScoring;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "credit_check_eligibility")
public class CreditCheckEligibility {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "credit_type")
    private CreditScoring scoringType;

    @ElementCollection
    @CollectionTable(name = "credit_check_eligibility_notes",
            joinColumns = @JoinColumn(name = "credit_check_eligibility_id"))
    private List<String> notes;

    @OneToOne
    @JoinColumn(name = "eligibility_id")
    private Eligibility eligibility;
}
