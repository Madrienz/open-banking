package other;

import ccc.CccMarketingState;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "other_fee_charges")
public class OtherFeeCharges {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @OneToMany(mappedBy = "other_fee_charges_id")
    private List<FeeChargeDetail> feeChargeDetail;

    @OneToMany(mappedBy = "other_fee_charges_id")
    private List<FeeChargeCap> feeChargeCap;

    @OneToOne
    @JoinColumn(name = "ccc_marketing_state_id")
    private CccMarketingState cccMarketingState;
}
