package other;

import enums.FeeCategory;
import enums.FeeFrequency;
import enums.FeeType;
import enums.InterestRateType;
import lombok.Data;
import repayment.OtherFeeType;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "fee_charge_detail")
public class FeeChargeDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "fee_category", length = 4)
    private FeeCategory feeCategory;

    @Enumerated(EnumType.STRING)
    @Column(name = "fee_type", length = 4)
    private FeeType feeType;

    @Column(name = "negotiable_indicator")
    private boolean negotiableIndicator;

    @Column(name = "included_in_periodic_fee_indicator")
    private boolean includedInPeriodicFeeIndicator;

    @Column(name = "fee_amount")
    private String feeAmount;

    @Column(name = "fee_rate")
    private String feeRate;

    @Enumerated(EnumType.STRING)
    @Column(name = "fee_rate_type", length = 4)
    private InterestRateType feeRateType;

    @Enumerated(EnumType.STRING)
    @Column(name = "application_frequency", length = 4)
    private FeeFrequency applicationFrequency;

    @Enumerated(EnumType.STRING)
    @Column(name = "calculation_frequency", length = 4)
    private FeeFrequency calculationFrequency;

    @ElementCollection
    @CollectionTable(name = "fee_charge_detail_notes",
            joinColumns = @JoinColumn(name = "fee_charge_detail_id"))
    private List<String> notes;

    @ElementCollection
    @CollectionTable(name = "fee_charge_detail_other_fee_category",
            joinColumns = @JoinColumn(name = "fee_charge_detail_id"))
    private OtherCodeType otherFeeCategory;

    @ElementCollection
    @CollectionTable(name = "fee_charge_detail_other_fee_type",
            joinColumns = @JoinColumn(name = "fee_charge_detail_id"))
    private OtherFeeType otherFeeType;

    @ElementCollection
    @CollectionTable(name = "fee_charge_detail_other_fee_rate_type",
            joinColumns = @JoinColumn(name = "fee_charge_detail_id"))
    private OtherCodeType otherFeeRateType;

    @ElementCollection
    @CollectionTable(name = "fee_charge_detail_other_application_frequency",
            joinColumns = @JoinColumn(name = "fee_charge_detail_id"))
    private OtherCodeType otherApplicationFrequency;

    @ElementCollection
    @CollectionTable(name = "fee_charge_detail_other_calculation_frequency",
            joinColumns = @JoinColumn(name = "fee_charge_detail_id"))
    private OtherCodeType otherCalculationFrequency;

    @OneToOne
    @JoinColumn(name = "fee_applicable_range")
    private FeeApplicableRange feeApplicableRange;

    @ManyToOne
    @JoinColumn(name = "other_fee_charges_id")
    private OtherFeeCharges otherFeeCharges;
}
