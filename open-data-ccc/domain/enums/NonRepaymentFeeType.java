package enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;

@JsonFormat
public enum NonRepaymentFeeType {
    NRLP("LatePayment",
            "Fee/rate charged for failing to pay the minimum payment amount by the agreed date"),
    NROL("OverCreditLimit",
            "Fee/rate charged for usage of transactions beyond the card credit limit"),
    NROT("Other",
            "Any other fees/rates related to non-repayment"),
    NRRP("ReturnPayment",
            "Fee/rate charged for returned payment");

    public final String codeName;
    public final String description;

    NonRepaymentFeeType(String codeName, String description) {
        this.codeName = codeName;
        this.description = description;
    }

    @JsonValue
    public String getName() {
        return codeName;
    }

    @JsonCreator
    public static NonRepaymentFeeType fromName(String codeName) {
        for (NonRepaymentFeeType nonRepaymentFeeType : NonRepaymentFeeType.values()) {
            if (nonRepaymentFeeType.codeName.equals(codeName)) {
                return nonRepaymentFeeType;
            }
        }
        throw new IllegalArgumentException("No such NonRepaymentFeeType.");
    }
}
