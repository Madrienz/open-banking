package enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;

@JsonFormat
public enum ProductSegment {
    GEG("General", "Covers all commercial credit cards"),
    SGOG("Other", "Covers all commercial credit cards");

    public final String codeName;
    public final String description;

    ProductSegment(String codeName, String description) {
        this.codeName = codeName;
        this.description = description;
    }

    @JsonValue
    public String getName() {
        return codeName;
    }

    @JsonCreator
    public static ProductSegment fromName(String codeName) {
        for (ProductSegment productSegment : ProductSegment.values()) {
            if (productSegment.codeName.equals(codeName)) {
                return productSegment;
            }
        }
        throw new IllegalArgumentException("No such ProductSegment.");
    }
}
