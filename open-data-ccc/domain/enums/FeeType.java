package enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;

@JsonFormat
public enum FeeType {
    FEAC("AdditionalCard", "Fee charged for each additional card issued"),
    FEBT("BalanceTransfer", "Fee/Rate charged for outstanding balance transferred " +
            "from another provider"),
    FECA("CashAdvance", "Fee/rate charged for cash advance on card. " +
            "Note: This is broader than just a cash withdrawal."),
    FECD("Card", "Fee charged for each card issued"),
    FECI("ChequeIssue", "Fee/rate charged on cheques issued"),
    FECW("CashWithdrawal", "Fee/rate charged for cash withdrawal only on card"),
    FEEC("EmergencyCard", "Fee/rate charged for issuance of emergency card"),
    FEFC("ForeignCash", "Fee/rate charged for transactions in currencies other than sterling"),
    FEHD("Handling", "Fee/rate charged for handling"),
    FEMA("Maintenance", "Fee/rate charged for maintenance of card"),
    FEOT("Other", "Any other fees/rates not covered"),
    FEPU("Purchase", "Fee/rate charged for purchasing goods or services via a credit card"),
    FEPY("Penalty", "Fees charged as penalties e.g. Late or Returned payment");

    public final String codeName;
    public final String description;

    FeeType(String codeName, String description) {
        this.codeName = codeName;
        this.description = description;
    }

    @JsonValue
    public String getName() {
        return codeName;
    }

    @JsonCreator
    public static FeeType fromName(String codeName) {
        for (FeeType feeType : FeeType.values()) {
            if (feeType.codeName.equals(codeName)) {
                return feeType;
            }
        }
        throw new IllegalArgumentException("No such FeeType.");
    }
}
