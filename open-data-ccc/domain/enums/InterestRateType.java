package enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;

@JsonFormat
public enum InterestRateType {
    INGR("Gross",
            "Interest rate shown is before any tax deducted"),
    INOT("Other",
            "Covers any other type of interest that the bank may use. Fill out the OtherInterestRateType block.");

    public final String codeName;
    public final String description;

    InterestRateType(String codeName, String description) {
        this.codeName = codeName;
        this.description = description;
    }

    @JsonValue
    public String getName() {
        return codeName;
    }

    @JsonCreator
    public static InterestRateType fromName(String codeName) {
        for (InterestRateType interestRateType : InterestRateType.values()) {
            if (interestRateType.codeName.equals(codeName)) {
                return interestRateType;
            }
        }
        throw new IllegalArgumentException("No such InterestRateType.");
    }
}
