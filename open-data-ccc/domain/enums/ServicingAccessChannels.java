package enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;

@JsonFormat
public enum ServicingAccessChannels {
    CAAT("ATM", "Account holder can perform their banking activities using an ATM"),
    CABR("Branch", "Account holder can visit a branch to perform banking activities"),
    CACC("CallCentre", "Account holder can call a call centre to perform banking activities"),
    CALE("Post", "Account holder can perform their banking activities via post"),
    CAMB("MobileBankingApp", "Account holder can use a mobile banking app " +
            "to conduct business with the bank"),
    CAON("Online", "Account holder can perform their banking activities online using a web browser"),
    CAPO("PostOffice", "Account holder can visit a post office to perform banking activities"),
    CARM("RelationshipManager", "Account holder communicates with a relationship manager " +
            "who can perform banking activities on their behalf."),
    CATX("Text", "Account holder can perform their banking activities using mobile phone texts");

    public final String codeName;
    public final String description;

    ServicingAccessChannels(String codeName, String description) {
        this.codeName = codeName;
        this.description = description;
    }

    @JsonValue
    public String getName() {
        return codeName;
    }

    @JsonCreator
    public static ServicingAccessChannels fromName(String codeName) {
        for (ServicingAccessChannels servicingAccessChannels : ServicingAccessChannels.values()) {
            if (servicingAccessChannels.codeName.equals(codeName)) {
                return servicingAccessChannels;
            }
        }
        throw new IllegalArgumentException("No such ServicingAccessChannels.");
    }
}
