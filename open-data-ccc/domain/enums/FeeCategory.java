package enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;

@JsonFormat
public enum FeeCategory {
    FCAT("CashAdvance", "Charges associated with Cash advances"),
    FCBT("BalanceTransfer", "Charges associated with balance transfers"),
    FCCD("Purchase", "Charges associated with Purchases"),
    FCCQ("Cheque", "Charges associated with credit card cheques"),
    FCCR("Card", "Any charges related to issue or maintenance of card"),
    FCFX("FX", "Charges associated with foreign transactions made on credit card"),
    FCPY("Penalty", "Fees charged as penalties e.g. Late or Returned payment"),
    FCSV("Servicing", "Any maintenance/handling/servicing fees"),
    FCTR("Other", "Any other charges associated with the commercial credit card");

    public final String codeName;
    public final String description;

    FeeCategory(String codeName, String description) {
        this.codeName = codeName;
        this.description = description;
    }

    @JsonValue
    public String getName() {
        return codeName;
    }

    @JsonCreator
    public static FeeCategory fromName(String codeName) {
        for (FeeCategory feeCategory : FeeCategory.values()) {
            if (feeCategory.codeName.equals(codeName)) {
                return feeCategory;
            }
        }
        throw new IllegalArgumentException("No such FeeCategory.");
    }
}
