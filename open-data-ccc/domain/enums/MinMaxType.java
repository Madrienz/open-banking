package enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;

@JsonFormat
public enum MinMaxType {
    FMMN("Minimum",
            "Indicates that this is the maximum fee/charge that can be applied by the financial institution"),
    FMMX("Maximum",
            "Indicates that this is the minimum fee/charge that can be applied by the financial institution");

    public final String codeName;
    public final String description;

    MinMaxType(String codeName, String description) {
        this.codeName = codeName;
        this.description = description;
    }

    @JsonValue
    public String getName() {
        return codeName;
    }

    @JsonCreator
    public static MinMaxType fromName(String codeName) {
        for (MinMaxType minMaxType : MinMaxType.values()) {
            if (minMaxType.codeName.equals(codeName)) {
                return minMaxType;
            }
        }
        throw new IllegalArgumentException("No such MinMaxType.");
    }
}
