package enums;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;

@JsonFormat
public enum EligibilityType {
    ELAB("AnyBusinessCustomer",
            "The card product is available to any business customer"),

    ELBC("BusinessCurrentAccount",
            "This card product is available to a business which has a current account" +
                    " in general (with the same bank or other banks)"),

    ELCS("CreditScoring",
            "This card product is available to a customer who has passed their credit check"),

    ELFH("OtherFinancialHolding",
            "The card product is available to business which has other financial holding"),

    ELOT("Other",
            "Fill out the OtherEligibilityType fields for any Eligibility criteria not covered"),

    ELTU("MinimumTurnover",
            "The business turnover must be over this level for the business to be eligible for this card");

    public final String codeName;
    public final String description;

    EligibilityType(String codeName, String description) {
        this.codeName = codeName;
        this.description = description;
    }

    @JsonValue
    public String getName() {
        return codeName;
    }

    @JsonCreator
    public static EligibilityType fromName(String codeName) {
        for (EligibilityType eligibilityType : EligibilityType.values()) {
            if (eligibilityType.codeName.equals(codeName)) {
                return eligibilityType;
            }
        }
        throw new IllegalArgumentException("No such EligibilityType.");
    }
}
