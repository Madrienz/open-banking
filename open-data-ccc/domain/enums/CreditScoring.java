package enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;

@JsonFormat
public enum CreditScoring {
    CAHA("Hard", "Hard credit scoring is used on account opening to determine an applicant's credit worthiness. " +
                 "This may affect the applicants credit rating."),
    CASF("Soft", "Soft pull credit scoring (quotation search) is used on account opening " +
            "to determine applicants credit worthiness. This does not affect the applicants credit rating.");

    public final String codeName;
    public final String description;

    CreditScoring(String codeName, String description) {
        this.codeName = codeName;
        this.description = description;
    }

    @JsonValue
    public String getName() {
        return codeName;
    }

    @JsonCreator
    public static CreditScoring fromName(String codeName) {
        for (CreditScoring creditScoring : CreditScoring.values()) {
            if (creditScoring.codeName.equals(codeName)) {
                return creditScoring;
            }
        }
        throw new IllegalArgumentException("No such CreditScoring.");
    }
}
