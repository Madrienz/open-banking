package repayment;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@Entity
@Table(name = "non_repayment_fee_charges")
public class NonRepaymentFeeCharges {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @OneToMany(mappedBy = "non_repayment_fee_charges_id")
    @Size(min = 1)
    private List<NonRepaymentFeeChargeDetail> nonRepaymentFeeChargeDetail;

    @OneToMany(mappedBy = "non_repayment_fee_charges_id")
    private List<NonRepaymentFeeChargeCap> nonRepaymentFeeChargeCap;

    @ManyToOne
    @JoinColumn(name = "repayment_id")
    private Repayment repayment;
}
