package repayment;

import enums.FeeCategory;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "other_fee_type")
public class OtherFeeType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "code", length = 4)
    private String code;

    @Enumerated(EnumType.STRING)
    @Column(name = "fee_category", length = 4, nullable = false)
    private FeeCategory feeCategory;

    @Column(name = "name", length = 70)
    private String name;

    @Column(name = "description", length = 350)
    private String description;

    @ManyToOne
    @JoinColumn(name = "no_repayment_fee_charge_detail_id")
    private NonRepaymentFeeChargeDetail nonRepaymentFeeChargeDetail;
}
