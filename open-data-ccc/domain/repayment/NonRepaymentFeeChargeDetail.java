package repayment;

import enums.FeeFrequency;
import enums.FeeType;
import enums.InterestRateType;
import lombok.Data;
import other.OtherCodeType;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "non_repayment_fee_charge_detail")
public class NonRepaymentFeeChargeDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "fee_type", length = 4, nullable = false)
    private FeeType feeType;

    @Column(name = "negotiable_indicator")
    private boolean negotiableIndicator;

    @Column(name = "fee_amount")
    private String feeAmount;

    @Column(name = "fee_rate")
    private String feeRate;

    @Column(name = "fee_rate_type")
    private InterestRateType feeRateType;

    @Enumerated(EnumType.STRING)
    @Column(name = "application_frequency", length = 4, nullable = false)
    private FeeFrequency applicationFrequency;

    @Enumerated(EnumType.STRING)
    @Column(name = "calculation_frequency", length = 4, nullable = false)
    private FeeFrequency calculationFrequency;

    @ElementCollection
    @CollectionTable(name = "non_repayment_fee_charge_detail_notes",
            joinColumns = @JoinColumn(name = "non_repayment_fee_charge_detail_id"))
    private List<String> notes;

    @OneToMany(mappedBy = "no_repayment_fee_charge_detail_id")
    private List<OtherFeeType> otherFeeType;

    @ElementCollection
    @CollectionTable(name = "other_fee_rate_type", joinColumns = @JoinColumn(name = "non_repayment_fee_charge_detail_id"))
    private List<OtherCodeType> otherFeeRateType;

    @ElementCollection
    @CollectionTable(name = "other_application_frequency", joinColumns = @JoinColumn(name = "non_repayment_fee_charge_detail_id"))
    private List<OtherCodeType> otherApplicationFrequency;

    @ElementCollection
    @CollectionTable(name = "other_calculation_frequency", joinColumns = @JoinColumn(name = "non_repayment_fee_charge_detail_id"))
    private List<OtherCodeType> otherCalculationFrequency;
}
