package domain.codes;

public enum ContactTypeCode {
	BRAE("AlternateEmail"),
	BRAF("AlternateFax"),
	BRAP("AlternatePhone"),
	BREM("Email"),
	BRFX("Fax"),
	BROT("Other"),
	BRPH("Phone");

	private String value;

	ContactTypeCode(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	@Override
	public String toString() {
		return value;
	}
}
