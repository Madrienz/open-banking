package domain;

import lombok.Data;

import javax.persistence.*;

@Data
@Embeddable
public class OtherContactType {

	@Column
	private String code;

	@Column(length = 70, nullable = false)
	private String name;

	@Column(length = 350, nullable = false)
	private String description;
}
