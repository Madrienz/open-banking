package domain;

import domain.codes.WeekDayCode;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@Entity
@Table(name = "availability_days")
public class Day {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Enumerated(EnumType.STRING)
	@Column(length = 4, nullable = false)
	private WeekDayCode name;

	@Column(length = 2000)
	private String notes;

	@ElementCollection
	@CollectionTable(name = "opening_hours",
			joinColumns = @JoinColumn(name = "day_id"))
	@Size(min = 1)
	private List<OpeningHours> openingHours;
}
