package domain;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "geo_locations")
public class GeoLocation {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Embedded
	private GeographicCoordinates geographicCoordinates;
}
