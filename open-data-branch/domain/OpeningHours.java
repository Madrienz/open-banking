package domain;

import java.time.OffsetTime;

import lombok.Data;

import javax.persistence.*;

@Data
@Embeddable
public class OpeningHours {

	@Column(nullable = false)
	private OffsetTime openingTime;

	@Column(nullable = false)
	private OffsetTime closingTime;
}
