package domain;

import domain.enumeration.PaymentContextCode;
import lombok.Data;

@Data
public class OBRisk1 {
    private PaymentContextCode paymentContextCode;
    private String merchantCategoryCode;
    private String merchantCustomerIdentification;
    private DeliveryAddress deliveryAddress;
}
