package domain;

import domain.enumeration.AppliedAuthenticationApproach;
import domain.enumeration.RequestedSCAExemptionType;
import lombok.Data;

@Data
public class OBSCASupportData1 {
    private RequestedSCAExemptionType requestedSCAExemptionType;
    private AppliedAuthenticationApproach authenticationApproach;
    private String referencePaymentOrderId;
}
