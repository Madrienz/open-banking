package domain.enumeration;

public enum RequestedSCAExemptionType {
    BILLPAYMENT("BillPayment"),
    CONTACTLESSTRAVEL("ContactlessTravel"),
    ECOMMERCEGOODS("EcommerceGoods"),
    ECOMERCESERVICES("EcommerceServices"),
    KIOSK("Kiosk"),
    PARKING("Parking"),
    PARTYTOPARTY("PartyToParty");

    private String value;

    RequestedSCAExemptionType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }

}
