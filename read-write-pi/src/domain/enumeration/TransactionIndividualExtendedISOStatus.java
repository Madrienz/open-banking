package domain.enumeration;

public enum TransactionIndividualExtendedISOStatus {
    ACCEPTED("Accepted"),

    ACCEPTEDCANCELLATIONREQUEST("AcceptedCancellationRequest"),

    ACCEPTEDCREDITSETTLEMENTCOMPLETED("AcceptedCreditSettlementCompleted"),

    ACCEPTEDCUSTOMERPROFILE("AcceptedCustomerProfile"),

    ACCEPTEDFUNDSCHECKED("AcceptedFundsChecked"),

    ACCEPTEDSETTLEMENTCOMPLETED("AcceptedSettlementCompleted"),

    ACCEPTEDSETTLEMENTINPROCESS("AcceptedSettlementInProcess"),

    ACCEPTEDTECHNICALVALIDATION("AcceptedTechnicalValidation"),

    ACCEPTEDWITHCHANGE("AcceptedWithChange"),

    ACCEPTEDWITHOUTPOSTING("AcceptedWithoutPosting"),

    CANCELLED("Cancelled"),

    NOCANCELLATIONPROCESS("NoCancellationProcess"),

    PARTIALLYACCEPTEDCANCELLATIONREQUEST("PartiallyAcceptedCancellationRequest"),

    PARTIALLYACCEPTEDTECHNICALCORRECT("PartiallyAcceptedTechnicalCorrect"),

    PAYMENTCANCELLED("PaymentCancelled"),

    PENDING("Pending"),

    PENDINGCANCELATTIONREQUEST("PendingCancellationRequest"),

    RECEIVED("Received"),

    REJECTED("Rejected"),

    REJECTEDCANCELLATIONREQUEST("RejectedCancellationRequest");

    private String value;

    TransactionIndividualExtendedISOStatus(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }
}
