package domain.enumeration;

public enum StatusReason {
    CANCELLED("Cancelled"),
    PENDINGFAILINGSETTLEMENT("PendingFailingSettlement"),
    PENDINGSETTLEMENT("PendingSettlement"),
    PROPRIETARY("Proprietary"),
    PROPRIETARYREJECTION("ProprietaryRejection"),
    SUSPENDED("Suspended"),
    UNMATCHED("Unmatched");


    private String value;

    StatusReason(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }
}
