package domain;

import domain.codes.PeriodCode;
import lombok.Data;

import java.util.List;

@Data
public class RepaymentHoliday {

    private int maxHolidayLength;
    private PeriodCode maxHolidayPeriod;
    private List<String> notes;

}
