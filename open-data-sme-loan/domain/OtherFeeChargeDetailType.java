package domain;

import domain.codes.FeeCategoryCode;
import lombok.Data;

@Data
public class OtherFeeChargeDetailType {

    private String code;
    private FeeCategoryCode feeCategory;
    private String name;
    private String description;

}
