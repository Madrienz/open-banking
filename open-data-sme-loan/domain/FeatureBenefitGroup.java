package domain;

import domain.codes.FeatureBenefitTypeCode;
import domain.codes.FrequencyCode;
import lombok.Data;

import java.util.List;

@Data
public class FeatureBenefitGroup {

    private String name;
    private FeatureBenefitTypeCode type;
    private String benefitGroupNominalValue;
    private String fee;
    private FrequencyCode applicationFrequency;
    private FrequencyCode calculationFrequency;
    private List<String> notes;
    private OtherCodeType otherType;
    private OtherCodeType otherApplicationFrequency;
    private OtherCodeType otherCalculationFrequency;
    private List<FeatureBenefitItem> featureBenefitItem;
    private List<FeatureBenefitEligibility> featureBenefitEligibility;

}
