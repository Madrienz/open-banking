package domain.codes;

public enum LegalStructureCode {
    LSCI("CIO"),
    LSCS("ClubSociety"),
    LSCY("Charity"),
    LSLD("Ltd"),
    LSLG("LBG"),
    LSLP("LLP"),
    LSOT("Other"),
    LSPP("Partnership"),
    LSST("SoleTrader"),
    LSTT("Trust");

    private String value;

    LegalStructureCode(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }
}
