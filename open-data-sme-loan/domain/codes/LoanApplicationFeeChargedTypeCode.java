package domain.codes;

public enum LoanApplicationFeeChargedTypeCode {
    LACA("ChargedAfterLoanApproval"),
    LACI("ChargedIrrespectiveOfLoanApproval"),
    LANF("NoLoanApplicationFee");

    private String value;

    LoanApplicationFeeChargedTypeCode(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }
}
