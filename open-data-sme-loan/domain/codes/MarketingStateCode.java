package domain.codes;

public enum MarketingStateCode {
    CAPR("Promotional"),
    CARE("Regular");

    private String value;

    MarketingStateCode(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }
}
