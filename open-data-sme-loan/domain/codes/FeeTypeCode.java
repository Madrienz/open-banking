package domain.codes;

public enum FeeTypeCode {
    FEAP("Application"),
    FEAR("Arrangement"),
    FECC("FeeChargeCap"),
    FECR("CreditReference"),
    FEER("EarlyRepayment"),
    FELP("LatePayment"),
    FEMF("MissedPaymentFee"),
    FEMO("Monthly"),
    FEMP("MissedPaymentRate"),
    FEOP("OtherPaymentFee"),
    FEOT("Other"),
    FEPF("PrepaymentFee"),
    FEPR("OtherPaymentRate"),
    FERS("Re-statement"),
    FEST("Statement"),
    FESU("SetUp");

    private String value;

    FeeTypeCode(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }
}
