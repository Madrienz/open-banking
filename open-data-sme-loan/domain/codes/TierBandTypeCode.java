package domain.codes;

public enum TierBandTypeCode {
    TIBA("Banded"),
    TIIN("Tiered"),
    TIWH("Whole");

    private String value;

    TierBandTypeCode(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }
}
