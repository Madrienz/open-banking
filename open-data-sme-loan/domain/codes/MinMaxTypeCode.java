package domain.codes;

public enum MinMaxTypeCode {
    FMMN("Minimum"),
    FMMX("Maximum");

    private String value;

    MinMaxTypeCode(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }
}
