package domain.codes;

public enum FrequencyCode {
    FQDY("Daily"),
    FQHY("HalfYearly"),
    FQMY("Monthly"),
    FQOT("Other"),
    FQQY("Quarterly"),
    FQWY("Weekly"),
    FQYY("Yearly");

    private String value;

    FrequencyCode(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }
}
