package domain.codes;

public enum SalesAccessChannelsCode {
    USBR("Branch"),
    USCC("CallCentre"),
    USLE("Post"),
    USON("Online"),
    USPO("PostOffice"),
    USRM("RelationshipManager");

    private String value;

    SalesAccessChannelsCode(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }
}
