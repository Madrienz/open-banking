package domain.codes;

public enum ServicingAccessChannelsCode {
    CAAT("ATM"),
    CABR("Branch"),
    CACC("CallCentre"),
    CALE("Post"),
    CAMB("MobileBankingApp"),
    CAON("Online"),
    CAPO("PostOffice"),
    CARM("RelationshipManager"),
    CATX("Text");

    private String value;

    ServicingAccessChannelsCode(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }
}
