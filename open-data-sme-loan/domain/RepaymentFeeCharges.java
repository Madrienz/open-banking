package domain;

import lombok.Data;

import java.util.List;

@Data
public class RepaymentFeeCharges {

    private List<RepaymentFeeChargeDetail> repaymentFeeChargeDetail;
    private List<RepaymentFeeChargeCap> repaymentFeeChargeCap;

}
