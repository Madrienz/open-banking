package domain;

import domain.codes.PeriodCode;
import domain.codes.SMELEligibilityTypeCode;
import lombok.Data;

import java.util.List;

@Data
public class FeatureBenefitEligibility {

    private String name;
    private String description;
    private SMELEligibilityTypeCode type;
    private String amount;
    private boolean indicator;
    private String textual;
    private PeriodCode period;
    private List<String> notes;
    private OtherCodeType otherType;

}
