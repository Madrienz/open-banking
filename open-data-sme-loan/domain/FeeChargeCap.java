package domain;

import domain.codes.FeeTypeCode;
import domain.codes.MinMaxTypeCode;
import domain.codes.PeriodCode;
import lombok.Data;

import java.util.List;
import java.util.Set;

@Data
public class FeeChargeCap {

    private Set<FeeTypeCode> feeType;
    private MinMaxTypeCode minMaxType;
    private int feeCapOccurrence;
    private String feeCapAmount;
    private PeriodCode cappingPeriod;
    private List<String> notes;
    private Set<OtherCodeType> otherFeeType;

}
