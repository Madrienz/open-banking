package domain;

import domain.codes.FeeFrequencyCode;
import domain.codes.FeeTypeCode;
import domain.codes.MinMaxTypeCode;
import lombok.Data;

import java.util.List;
import java.util.Set;

@Data
public class LoanInterestFeeChargeCap {

    private Set<FeeTypeCode> feeType;
    private MinMaxTypeCode minMaxType;
    private int feeCapOccurrence;
    private String feeCapAmount;
    private FeeFrequencyCode cappingPeriod;
    private List<String> notes;
    private Set<OtherCodeType> otherFeeType;

}
