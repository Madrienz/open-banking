package domain;

import lombok.Data;

import java.util.List;

@Data
public class IDEligibility {

    private String uRL;
    private List<String> notes;

}
