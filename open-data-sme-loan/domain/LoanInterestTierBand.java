package domain;

import domain.codes.InterestFixedVariableTypeCode;
import domain.codes.InterestRateTypeCode;
import domain.codes.PeriodCode;
import lombok.Data;

import java.util.List;

@Data
public class LoanInterestTierBand {

    private String identification;
    private String tierValueMinimum;
    private String tierValueMaximum;
    private int tierValueMinTerm;
    private PeriodCode minTermPeriod;
    private int tierValueMaxTerm;
    private PeriodCode maxTermPeriod;
    private InterestFixedVariableTypeCode fixedVariableInterestRateType;
    private String repAPR;
    private InterestRateTypeCode loanProviderInterestRateType;
    private String loanProviderInterestRate;
    private List<String> notes;
    private OtherCodeType otherLoanProviderInterestRateType;
    private List<LoanInterestFeesCharge> loanInterestFeesCharges;

}
