package domain;

import lombok.Data;

import java.util.List;

@Data
public class Eligibility {

    private List<OtherEligibility> otherEligibility;
    private AgeEligibility ageEligibility;
    private List<ResidencyEligibility> residencyEligibility;
    private List<TradingHistoryEligibility> tradingHistoryEligibility;
    private List<LegalStructureEligibility> legalStructureEligibility;
    private List<OfficerEligibility> officerEligibility;
    private IDEligibility iDEligibility;
    private CreditCheckEligibility creditCheckEligibility;
    private IndustryEligibility industryEligibility;

}
