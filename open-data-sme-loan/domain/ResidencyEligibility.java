package domain;

import domain.codes.ResidencyTypeCode;
import lombok.Data;

import java.util.List;
import java.util.Set;

@Data
public class ResidencyEligibility {

    private ResidencyTypeCode residencyType;
    private Set<String> residencyIncluded;
    private List<String> notes;
    private OtherCodeType otherResidencyType;

}
