package domain;

import domain.codes.RepaymentFrequencyCode;
import domain.codes.SMELEligibilityTypeCode;
import lombok.Data;

import java.util.List;

@Data
public class OtherEligibility {

    private String name;
    private String description;
    private SMELEligibilityTypeCode type;
    private String amount;
    private boolean indicator;
    private String textual;
    private RepaymentFrequencyCode period;
    private List<String> notes;
    private OtherCodeType otherType;

}
