package domain;

import domain.codes.RepaymentAmountTypeCode;
import domain.codes.RepaymentFrequencyCode;
import domain.codes.RepaymentTypeCode;
import lombok.Data;

import java.util.List;

@Data
public class Repayment {

    private RepaymentTypeCode repaymentType;
    private RepaymentFrequencyCode repaymentFrequency;
    private RepaymentAmountTypeCode amountType;
    private List<String> notes;
    private OtherCodeType otherRepaymentType;
    private OtherCodeType otherRepaymentFrequency;
    private OtherCodeType otherAmountType;
    private RepaymentFeeCharges repaymentFeeCharges;
    private List<RepaymentHoliday> repaymentHoliday;

}
