package domain;

import domain.codes.InterestCalculationMethodCode;
import domain.codes.TierBandTypeCode;
import lombok.Data;

import java.util.List;
import java.util.Set;

@Data
public class LoanInterestTierBandSet {

    private TierBandTypeCode tierBandMethod;
    private String identification;
    private InterestCalculationMethodCode calculationMethod;
    private List<String> notes;
    private Set<LoanInterestTierBand> loanInterestTierBand;
    private List<LoanInterestFeesCharge> loanInterestFeesCharges;

}
