package domain;

import lombok.Data;

import java.util.List;

@Data
public class LoanInterest {

    private List<String> notes;
    private List<LoanInterestTierBandSet> loanInterestTierBandSet;

}
