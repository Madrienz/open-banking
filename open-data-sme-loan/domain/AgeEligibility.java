package domain;

import lombok.Data;

import java.util.List;

@Data
public class AgeEligibility {

    private int minimumAge;
    private int maximumAge;
    private List<String> notes;

}
