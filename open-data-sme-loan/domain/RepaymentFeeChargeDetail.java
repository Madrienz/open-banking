package domain;

import domain.codes.FeeFrequencyCode;
import domain.codes.FeeTypeCode;
import domain.codes.InterestRateTypeCode;
import lombok.Data;

import java.util.List;

@Data
public class RepaymentFeeChargeDetail {

    private FeeTypeCode feeType;
    private boolean negotiableIndicator;
    private String feeAmount;
    private String feeRate;
    private InterestRateTypeCode feeRateType;
    private FeeFrequencyCode applicationFrequency;
    private FeeFrequencyCode calculationFrequency;
    private List<String> notes;
    private OtherFeeChargeDetailType otherFeeType;
    private OtherCodeType otherFeeRateType;
    private OtherCodeType otherApplicationFrequency;
    private OtherCodeType otherCalculationFrequency;

}
